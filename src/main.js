import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import VueResource from 'vue-resource'
import Embed from 'v-video-embed'
// import './registerServiceWorker'
// import SoftUIDashboard from "./soft-ui-dashboard";

// Archivos css para las escuelas
import "./assets/css/escuela_css.css";


Vue.config.productionTip = false
Vue.use(VueResource)
Vue.use(Embed);
// Vue.use(SoftUIDashboard);

if(process.env.NODE_ENV == 'development'){
  Vue.http.options.root = 'http://localhost:3012/'
}else{
  Vue.http.options.root = process.env.VUE_APP_RUTA_API_INBI;
}


  // Vue.http.options.root = process.env.VUE_APP_RUTA_API_INBI;


new Vue({
    router,
    store,
    vuetify,
    render: function(h) { return h(App) }
}).$mount('#app')