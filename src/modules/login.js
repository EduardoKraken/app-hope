import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'
import vuetify from '@/plugins/vuetify';
const  base64 = require('base-64');

export default {
  namespaced: true,

  state: {
    login: false,
    datosUsuario: null,
    usuario: null,
    grupo: null,
    recursos_usuario:null,
    calificaciones:null,
    grupos: null,
    recursos: null,
    escuela: null,
    avatar: null,
    idevaluacion:null
  },

  mutations: {
    LOGEADO(state, value) {
      state.login = value
    },
    
    DATOS_USUARIO(state, datosUsuario) {
      state.datosUsuario = datosUsuario
    },

    USUARIO(state, usuario) {
      state.usuario = usuario
    },

    SALIR(state) {
      state.login            = false
      state.datosUsuario     = null
      state.usuario          = null
      state.grupo            = null
      state.recursos_usuario = null
      state.calificaciones   = null
      state.grupos           = null
      state.recursos         = null
      state.avatar           = null
      state.idevaluacion     = null
    },

    VER_GRUPO( state, grupo ){
      state.grupo = grupo
    },

    VER_RECURSOS_GRUPO_USUARIO( state, recursos_usuario ){
      state.recursos_usuario = recursos_usuario
    },

    VER_CALIFICACIONES( state, calificaciones){
      state.calificaciones   = calificaciones
    },

    VER_GRUPOS( state, grupos){
      state.grupos = grupos
    },

    VER_RECURSO( state, recursos){
      state.recursos = recursos
    },

    VER_ESCUELA( state, escuela){
      state.escuela = escuela
    },

    VER_AVATAR( state, avatar ){
      state.avatar = avatar
    },

    VER_GRUPO_TEACHER( state, grupo ){
      state.grupo = grupo
    },

    GUARDAR_ID_EVALUACION( state, idevaluacion ){
      state.idevaluacion = idevaluacion
    },
  },

  actions: {
          
    grabarUsuario({ commit, dispatch }, usuario) {
      dispatch('grabarDatosUsuario',  usuario)
      commit('USUARIO',               usuario)
      commit('LOGEADO',               true)

      // Validar el tipo de usuario
      if(usuario.idniveles == 6 ){
        router.push({ name: 'GruposAlumnos' })
      }

      if( usuario.idniveles == 5 ){
        router.push({ name: 'PerfilTeacher' })
      }

      if( usuario.idniveles == 'EDITOR'){
        router.push({ name: 'IngresarSucursal' })
      }
    },

    salirLogin({ commit }) {
      commit('SALIR')
      router.push({ name: 'Carga' })
    },

    grabarDatosUsuario({ commit, dispatch }, datos_usuario) {
      commit('DATOS_USUARIO', datos_usuario)
    },

    grabarGruposUsuario({ commit, dispatch }, grupos) {
      commit('VER_GRUPOS', grupos)
    },

    grabarAvatar({ commit, dispatch }, avatar) {
      commit('VER_AVATAR', avatar)
    },

    abrirGrupo( {commit, dispatch }, grupo){
      // Lo codificiamos a baseg4
      const id_grupo = base64.encode(grupo.id);

      return new Promise((resolve, reject) => {
        Vue.http.get('grupos.informacion/' + id_grupo).then( response => {
          commit('VER_GRUPO', response.body)
          router.push({ name: 'DetalleGrupo' })
          resolve( true )
        }).catch( error => {
          reject( error )
        })

      })
    },

    abrirGrupoTeacher( {commit, dispatch }, grupo){
      // VER LALISTA DE ASISTENCIA DE LOS ALUMNOS
      return new Promise((resolve, reject) => {
        commit('VER_GRUPO_TEACHER', grupo )
        router.push({ name: 'ListaAsistencia' })
      })
    },

    abrirRecursosAlumno( {commit, dispatch }, payload){
      // Lo codificiamos a baseg4
      return new Promise((resolve, reject) => {
        Vue.http.post('recursos.usuario', payload).then( response => {
          commit('VER_RECURSOS_GRUPO_USUARIO', response.body)
          if( payload.tipo == 1)
            router.push({ name: 'RecursosGrupoCerti' })
          else
            router.push({ name: 'RecursosGrupoUsuario' })
          resolve( true )
        }).catch( error => {
          reject( error )
        })
      })
    },

    abrirCalificaciones( {commit, dispatch }, payload){
      // Lo codificiamos a baseg4
      return new Promise((resolve, reject) => {
        Vue.http.post('calificacion.grupo', payload).then( response => {
          commit('VER_CALIFICACIONES', response.body)
          router.push({ name: 'Calificaciones' })
          resolve( true )
        }).catch( error => {
          reject( error )
        })
      })
    },

    abrirVentanaRecurso( {commit}, payload ){
      commit('VER_RECURSO', payload.recursosPorAbrir)
      switch( payload.ventana ){
        // Ejercicios
        case 1:
          router.push({name: 'Ejercicio'})
        break;

        // Exámenes
        case 2:
          router.push({name: 'Examen'})
        break;

        // PDF
        case 3:
          router.push({name: 'Books'})
        break;

        // Videos
        case 4:
          router.push({name: 'Videos'})
        break;
      }
    },

    guardar_id_evaluacion_exci( {commit, dispatch }, idevaluacion){
      commit('GUARDAR_ID_EVALUACION', idevaluacion)
      router.push({ name: 'Evaluacion' })
    },

    guardar_id_evaluacion_exci_ejercicio( {commit, dispatch }, idevaluacion){
      commit('GUARDAR_ID_EVALUACION', idevaluacion)
      router.push({ name: 'EjercicioExci' })
    },
  },

  getters: {
    getLogeadoLMS(state) {
      return state.login
    },

    getUsuarioLMS(state) {
      return state.usuario
    },

    getDatosUsuarioLMS(state) {
      return state.datosUsuario
    },

    getGrupo ( state ) {
      return state.grupo
    },

    getRecursosGrupoUsuario ( state ) {
      return state.recursos_usuario
    },

    getCalificaciones ( state ) {
      return state.calificaciones
    },

    getGrupos ( state ) {
      return state.grupos
    },

    getRecurso ( state ){
      return state.recursos
    },

    getEscuela ( state ){
      return state.escuela
    },

    getAvatar ( state ){
      return state.avatar
    },

    getIdEvaluacion ( state ){
      return state.idevaluacion
    }
  }
}